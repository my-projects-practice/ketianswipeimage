//
//  Const.swift
//  KeTian
//
//  Created by 金鑫 on 2020/8/26.
//  Copyright © 2020 金鑫. All rights reserved.
//

import Foundation

enum KTUISize {
    enum Cell {
        static let width: CGFloat = 300
        static let height: CGFloat = 375
    }
}
