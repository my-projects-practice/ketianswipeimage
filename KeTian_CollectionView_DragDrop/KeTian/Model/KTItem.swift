//
//  KTItem.swift
//  KeTian
//
//  Created by 金鑫 on 2020/8/26.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit

struct KTItem: Codable {
    let title: String
    let img: String
    let thumb: String
    let thumbWidth: CGFloat
    let thumbHeight: CGFloat
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case img = "img"
        case thumb = "thumb"
        case thumbWidth = "thumbWidth"
        case thumbHeight = "thumbHeight"
    }
    
    var trimTitle: String {
        title.replacingOccurrences(of: "<em>", with: "").replacingOccurrences(of: "</em>", with: "")
    }
}
