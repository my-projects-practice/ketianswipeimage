//
//  KTSwipeCardController.swift
//  KeTian
//
//  Created by 金鑫 on 2020/8/26.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit
import SnapKit

enum KTTestWay {
    enum Update: String {
        case part
        case all
    }
}

class KTSwipeCardController: UIViewController {
    
    static let testWay = KTTestWay.Update.all
    
    var dragIndexPath: IndexPath?
    var currentIndex: Int = 0
    
    lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar(frame: CGRect.zero)
        searchBar.delegate = self
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "请输入物品名称"
        searchBar.becomeFirstResponder()
        
        return searchBar
    }()
    
    lazy var swipeCardView = KTSwipeCardView(target: self)
    
    lazy var goodsArray = [KTItem]()
    lazy var dataSource = [KTItem]()
//    var ob: NSKeyValueObservation?
   
    func wayPartRefreshData() {
        let rowMin: Int = 3
        if dataSource.count == 0 {
            for _ in 0..<rowMin {
                objc_sync_enter(self)
                guard currentIndex >= 0 else {
                    objc_sync_exit(self)
                    return
                }
                dataSource.append(goodsArray[currentIndex])
                currentIndex = currentIndex - 1
                objc_sync_exit(self)
            }
            swipeCardView.reloadData()
        }
    }
    
    func wayPartUpdateData() {
        objc_sync_enter(self)
        currentIndex = goodsArray.count - 1
        dataSource.removeAll()
        objc_sync_exit(self)
    }
    
    func wayAllRefreshData() {
        objc_sync_enter(self)
        dataSource.removeAll()
        dataSource.append(contentsOf: goodsArray)
        objc_sync_exit(self)
        swipeCardView.reloadData()
        print("swipeCardView.reloadData")

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        initOb()
        setupUI()
        layoutUI()
    }

}

// MARK: - setupUI and layoutUI
extension KTSwipeCardController {
    
    func setupUI() {
        view.addSubview(searchBar)
        view.addSubview(swipeCardView)
    }
    
    func layoutUI() {
        searchBar.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(21)
            make.left.right.equalToSuperview()
            make.height.equalTo(64)
        }
        
        swipeCardView.snp.makeConstraints { (make) in
            make.top.equalTo(searchBar.snp.bottom)
            make.left.right.bottom.equalToSuperview()
        }
    }
    
}

// MARK: - UICollectionViewDataSource
extension KTSwipeCardController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: KTSwipeCardView.KTSwipeCardViewCellId, for: indexPath) as! KTSwipeCardViewCell
        cell.item = dataSource[indexPath.item]
        print("cell \(indexPath)")

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        if (indexPath.item == 0) {
            return true
        }
        return false
    }
    
}

// MARK: - UICollectionViewDelegate
extension KTSwipeCardController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        searchBar.resignFirstResponder()
        objc_sync_enter(self)
        dataSource.remove(at: indexPath.item)
        swipeCardView.deleteItems(at: [indexPath])
        objc_sync_exit(self)
        
        if KTSwipeCardController.testWay == KTTestWay.Update.part {
            wayPartRefreshData()
        }
        print("dataSource.count: \(dataSource.count)")
    }
    
}

// MARK: - KTRefreshDelegate
extension KTSwipeCardController: KTRefreshDelegate {
    
    func refreshAllImages() {
        if KTSwipeCardController.testWay == KTTestWay.Update.all {
            wayAllRefreshData()
        } else if KTSwipeCardController.testWay == KTTestWay.Update.part {
            wayPartUpdateData()
            wayPartRefreshData()
        }
    }
}

// MARK: - UICollectionViewDragDelegate
extension KTSwipeCardController: UICollectionViewDragDelegate {
    
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        searchBar.resignFirstResponder()
        print("Drag index: \(indexPath)")
        let dragItem = self.dragItem(forPhotoAt: IndexPath(item: indexPath.item, section: 0))
        dragIndexPath = indexPath
        return [dragItem]
    }
    
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
        let dragItem = self.dragItem(forPhotoAt: IndexPath(item: dataSource.count-1, section: 0))
        return [dragItem]
    }

    private func dragItem(forPhotoAt indexPath: IndexPath) -> UIDragItem {
        let item = self.dataSource[indexPath.item]
        let itemProvider = NSItemProvider(object: item.thumb as NSItemProviderWriting)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        dragItem.localObject = item.thumb
        
        return dragItem
    }
    
    func collectionView(_ collectionView: UICollectionView, dragPreviewParametersForItemAt indexPath: IndexPath) -> UIDragPreviewParameters? {
        let parameters = UIDragPreviewParameters()
        let rect = CGRect(x: 0, y: 0, width: KTUISize.Cell.width, height: KTUISize.Cell.height)
        parameters.visiblePath = UIBezierPath(roundedRect: rect, cornerRadius: 10)
        
        return parameters
    }
}

// MARK: - UICollectionViewDropDelegate
extension KTSwipeCardController: UICollectionViewDropDelegate {
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        let destinationIndexPath = coordinator.destinationIndexPath ?? IndexPath(item: 0, section: 0)
        guard let dragItem = coordinator.items.first?.dragItem else { return }
//        let ktItem = dataSource[dragIndexPath?.item ?? 0]
        if (self.dragIndexPath?.section == destinationIndexPath.section && self.dragIndexPath?.item == destinationIndexPath.item) {
                    return
                }

        collectionView.performBatchUpdates({
            objc_sync_enter(self)
            dataSource.remove(at: dragIndexPath?.item ?? 0)
            objc_sync_exit(self)
            swipeCardView.deleteItems(at: [dragIndexPath ?? IndexPath(item: 0, section: 0)])
        })
        coordinator.drop(dragItem, toItemAt: destinationIndexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        return UICollectionViewDropProposal(operation: .move, intent: .insertIntoDestinationIndexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidEnter session: UIDropSession) {
        objc_sync_enter(self)
        dataSource.remove(at: dragIndexPath?.item ?? 0)
        swipeCardView.deleteItems(at: [dragIndexPath ?? IndexPath(item: 0, section: 0)])
        objc_sync_exit(self)

    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidEnd session: UIDropSession) {
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidExit session: UIDropSession) {
        
    }
    
}

// MARK: - UISearchBarDelegate
extension KTSwipeCardController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.becomeFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        guard let text = searchBar.text, !text.isEmpty else { return }
        KTUtils.shared.searchGoods(goods: text) { [weak self] (success, items) in
        
            guard success == true, let items = items else { return }
            
            if KTSwipeCardController.testWay == KTTestWay.Update.all {
                objc_sync_enter(self ?? 0)
                self?.goodsArray.removeAll()
                self?.dataSource.removeAll()
                self?.goodsArray.append(contentsOf: items.reversed())
                self?.dataSource.append(contentsOf: self?.goodsArray ?? [KTItem]())
                objc_sync_enter(self ?? 0)
                self?.swipeCardView.reloadData()
                print("swipeCardView.reloadData")
                
            } else if KTSwipeCardController.testWay == KTTestWay.Update.part {
                objc_sync_enter(self ?? 0)
                self?.goodsArray.removeAll()
                self?.dataSource.removeAll()
                self?.goodsArray.append(contentsOf: items.reversed())
                self?.currentIndex = ((self?.goodsArray.count) ?? 0) - 1
                objc_sync_enter(self ?? 0)
                self?.wayPartRefreshData()
            }
        }
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
}
