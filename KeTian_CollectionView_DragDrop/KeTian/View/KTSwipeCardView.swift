//
//  KTSwipeCardView.swift
//  KeTian
//
//  Created by 金鑫 on 2020/8/26.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit
import Foundation

protocol KTRefreshDelegate: class {
    func refreshAllImages()
}

class KTSwipeCardView: UICollectionView {
    
    static let KTSwipeCardViewCellId = "KTSwipeCardViewCell"
    
    lazy var gesture = UIPanGestureRecognizer(target: self, action: #selector(refreshImage))
    
    weak var refreshDelegate: KTRefreshDelegate?
    
    init(target delegate: UICollectionViewDelegate & UICollectionViewDataSource & UICollectionViewDragDelegate & UICollectionViewDropDelegate & KTRefreshDelegate) {

        super.init(frame: CGRect.zero, collectionViewLayout: KTSwipeCardViewLayout())
        isPagingEnabled = true
        isScrollEnabled = true
        dragInteractionEnabled = true
        isSpringLoaded = true
        reorderingCadence = .immediate
        
        dataSource = delegate
        self.delegate = delegate
        dragDelegate = delegate
        dropDelegate = delegate
        refreshDelegate = delegate
        
        register(UINib.init(nibName: "KTSwipeCardViewCell", bundle: nil), forCellWithReuseIdentifier: KTSwipeCardView.KTSwipeCardViewCellId)
        backgroundColor = UIColor.lightGray
        
        addGestureRecognizer(gesture)

    }
    
    @objc func refreshImage() {
        print("refreshImage")
        refreshDelegate?.refreshAllImages()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var effectiveUserInterfaceLayoutDirection: UIUserInterfaceLayoutDirection {
        return .rightToLeft
    }

}
