//
//  UIScreen+Extension.swift
//  KeTian
//
//  Created by 金鑫 on 2020/8/27.
//  Copyright © 2020 金鑫. All rights reserved.
//

import Foundation

extension UIScreen {
    
    static var width : CGFloat  {
        return UIScreen.main.bounds.size.width
    }
    
    static var height : CGFloat  {
        return UIScreen.main.bounds.size.height
    }
    
    static var size : CGSize  {
        return UIScreen.main.bounds.size
    }
    
}
