//
//  KTUtils.swift
//  KeTian
//
//  Created by 金鑫 on 2020/8/26.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit
import Alamofire

class KTUtils: NSObject {
    
//  https://image.so.com/j?q=汉堡包&sn=0&pn=50
    let host: String = "https://image.so.com/j?"
    
    static let shared = KTUtils()
    
    func searchGoods(goods: String, callBack: @escaping (_ success: Bool, [KTItem]?) -> ()) {

        let url = host + "q=\(goods)" + "&sn=0&pn=50"
        let requestUrl = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        Alamofire.request(requestUrl).responseJSON { (response) in
            switch response.result {
            case .success:
                var goodsList: [KTItem]
                var jsonList: [Any]?
                
                guard let jsonData = response.data else {
                    callBack(false, nil)
                    return
                }
                
                do {
                    guard let jsonDict = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String : Any] else {
                        callBack(false, nil)
                        return
                    }
                    jsonList = jsonDict["list"] as? [Any]
                    guard jsonList != nil else {
                        callBack(false, nil)
                        return
                    }

                } catch {
                    callBack(false, nil)
                    return
                }
                
                do {
                    let data = try JSONSerialization.data(withJSONObject: jsonList as Any, options: [])
                    goodsList = try JSONDecoder().decode([KTItem].self, from: data)
                } catch {
                    callBack(false, nil)
                    return
                }
                
                callBack(true, goodsList)
                
            case .failure:
                callBack(false, nil)
            }
        }
    }
    
}
