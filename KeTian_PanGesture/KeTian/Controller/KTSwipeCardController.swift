//
//  KTSwipeCardController.swift
//  KeTian
//
//  Created by 金鑫 on 2020/8/26.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit
import SnapKit

enum KTTestWay {
    enum Update: String {
        case part
        case all
    }
}

class KTSwipeCardController: UIViewController {
    
    static let testWay = KTTestWay.Update.all
    
    var dragIndexPath: IndexPath?
    var currentIndex: Int = 0
    
    lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar(frame: CGRect.zero)
        searchBar.delegate = self
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "请输入物品名称"
        searchBar.becomeFirstResponder()
        
        return searchBar
    }()
    
    lazy var swipeCardView = KTSwipeCardView(target: self)
    
    lazy var goodsArray = [KTItem]()
    lazy var dataSource = [KTItem]()
//    var ob: NSKeyValueObservation?
   
    func wayPartRefreshData() {
        let rowMin: Int = 3
        if dataSource.count == 0 {
            for _ in 0..<rowMin {
                objc_sync_enter(self)
                guard currentIndex >= 0 else {
                    objc_sync_exit(self)
                    return
                }
                dataSource.append(goodsArray[currentIndex])
                currentIndex = currentIndex - 1
                objc_sync_exit(self)
            }
            swipeCardView.reloadData()
        }
    }
    
    func wayPartUpdateData() {
        objc_sync_enter(self)
        currentIndex = goodsArray.count - 1
        dataSource.removeAll()
        objc_sync_exit(self)
    }
    
    func wayAllRefreshData() {
        objc_sync_enter(self)
        dataSource.removeAll()
        dataSource.append(contentsOf: goodsArray)
        objc_sync_exit(self)
        swipeCardView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        initOb()
        setupUI()
        layoutUI()
    }

}

// MARK: - setupUI and layoutUI
extension KTSwipeCardController {
    
    func setupUI() {
        view.addSubview(searchBar)
        view.addSubview(swipeCardView)
    }
    
    func layoutUI() {
        searchBar.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(21)
            make.left.right.equalToSuperview()
            make.height.equalTo(64)
        }
        
        swipeCardView.snp.makeConstraints { (make) in
            make.top.equalTo(searchBar.snp.bottom)
            make.left.right.bottom.equalToSuperview()
        }
    }
    
}

// MARK: - UICollectionViewDataSource
extension KTSwipeCardController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: KTSwipeCardView.KTSwipeCardViewCellId, for: indexPath) as! KTSwipeCardViewCell
        cell.item = dataSource[indexPath.item]
        
        let longPress = UIPanGestureRecognizer(target: self, action: #selector(panGestureAction(gesture:)))
        cell.addGestureRecognizer(longPress)
        
        return cell
    }
    
    
}

// MARK: - UICollectionViewDelegate
extension KTSwipeCardController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        searchBar.resignFirstResponder()
        objc_sync_enter(self)
        dataSource.remove(at: indexPath.item)
        swipeCardView.deleteItems(at: [indexPath])
        objc_sync_exit(self)
        
        if KTSwipeCardController.testWay == KTTestWay.Update.part {
            wayPartRefreshData()
        }
    }
    
}

// MARK: - Gesture Actions
extension KTSwipeCardController {
    
    @objc private func panGestureAction(gesture: UIPanGestureRecognizer) {
        guard let cell = gesture.view,
            let indexPath = swipeCardView.indexPath(for: cell as! UICollectionViewCell) else { return }
        
        self.swipeCardView.bringSubviewToFront(cell)
        
        switch gesture.state {
        case .began:
            break
            
        case .changed:
            UIView.animate(withDuration: 0.25, animations: {
                cell.center = gesture.location(in: self.swipeCardView)
            })
            break
            
        case .cancelled:
            break
            
        case .ended:
            let center = swipeCardView.layoutAttributesForItem(at: indexPath)?.center ?? CGPoint(x: UIScreen.width * 0.5, y: UIScreen.height * 0.5 - 80)
            let isLeft = (cell.center.x - center.x) < 0 ? true : false
            let distX = abs(cell.center.x - center.x)
            let distY = abs(cell.center.y - center.y)
            
            if distX > UIScreen.width * 0.2 {
                UIView.animate(withDuration: 0.25, animations: {
                    if isLeft == true {
                        cell.center.x = -center.x
                    } else {
                        cell.center.x = UIScreen.width + cell.bounds.width * 0.5
                    }
                }, completion: { (_) in
                    objc_sync_enter(self)
                    self.dataSource.remove(at: indexPath.item)
                    (self.swipeCardView.collectionViewLayout as! KTSwipeCardViewLayout).attrsArray[indexPath.item].center = cell.center
                    self.swipeCardView.deleteItems(at: [indexPath])
                    objc_sync_exit(self)
                    if KTSwipeCardController.testWay == KTTestWay.Update.part {
                        self.wayPartRefreshData()
                    }
                })
                
            } else {
                UIView.animate(withDuration: 0.25, animations: {
                    cell.center = center
                }, completion: { (_) in
                    if distY > UIScreen.height * 0.2 {
                        self.wayAllRefreshData()
                    }
                })
            }
            break
            
        default:
            break
        }
        
    }
    
}

// MARK: - KTRefreshDelegate
extension KTSwipeCardController: KTRefreshDelegate {
    
    func refreshAllImages() {
        if KTSwipeCardController.testWay == KTTestWay.Update.all {
            wayAllRefreshData()
        } else if KTSwipeCardController.testWay == KTTestWay.Update.part {
            wayPartUpdateData()
            wayPartRefreshData()
        }
    }
}

// MARK: - UISearchBarDelegate
extension KTSwipeCardController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.becomeFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        guard let text = searchBar.text, !text.isEmpty else { return }
        KTUtils.shared.searchGoods(goods: text) { [weak self] (success, items) in
        
            guard success == true, let items = items else { return }
            
            if KTSwipeCardController.testWay == KTTestWay.Update.all {
                objc_sync_enter(self ?? 0)
                self?.goodsArray.removeAll()
                self?.dataSource.removeAll()
                self?.goodsArray.append(contentsOf: items.reversed())
                self?.dataSource.append(contentsOf: self?.goodsArray ?? [KTItem]())
                objc_sync_enter(self ?? 0)
                self?.swipeCardView.reloadData()
                
            } else if KTSwipeCardController.testWay == KTTestWay.Update.part {
                objc_sync_enter(self ?? 0)
                self?.goodsArray.removeAll()
                self?.dataSource.removeAll()
                self?.goodsArray.append(contentsOf: items.reversed())
                self?.currentIndex = ((self?.goodsArray.count) ?? 0) - 1
                objc_sync_enter(self ?? 0)
                self?.wayPartRefreshData()
            }
        }
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    
}
