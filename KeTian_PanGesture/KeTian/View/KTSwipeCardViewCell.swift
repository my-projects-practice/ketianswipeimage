//
//  KTSwipeCardViewCell.swift
//  KeTian
//
//  Created by 金鑫 on 2020/8/26.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit

class KTSwipeCardViewCell: UICollectionViewCell {

    @IBOutlet weak var goodsImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var item: KTItem? {
        didSet {
            goodsImageView.sd_setImage(with: URL(string: item?.thumb ?? ""), completed: nil)
            descriptionLabel.text = item?.trimTitle
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
    }
    
}
