//
//  KTSwipeCardViewLayout.swift
//  KeTian
//
//  Created by 金鑫 on 2020/8/26.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit

class KTSwipeCardViewLayout: UICollectionViewLayout {
    
    open lazy var attrsArray = [UICollectionViewLayoutAttributes]()
    
    override init() {
        super.init()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        super.prepare()
        
        attrsArray.removeAll()
        
        let count: Int = collectionView?.numberOfItems(inSection: 0) ?? 0
        for i in 0..<count {
            let indexPath = IndexPath(item: i, section: 0)
            guard let attrs = layoutAttributesForItem(at: indexPath) else { continue }
            attrsArray.append(attrs)
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return attrsArray
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attrs = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        attrs.size = CGSize(width: KTUISize.Cell.width, height: KTUISize.Cell.height)
        attrs.center = CGPoint(x: UIScreen.width * 0.5, y: UIScreen.height * 0.5 - 80)
        
        return attrs
    }
    
}
